# The {social_fishers} Repository: Social foraging can benefit artisanal fishers who interact with wild dolphins 

------------------------------------------------
#### Authors

Bruna Santos-Silva, Natalia Hanazaki, Fabio Daura-Jorge, Mauricio Cantor

#### Maintainer and contact: 
Mauricio Cantor

Department of Fisheries, Wildlife and Conservation Sciences, Oregon State University, USA

Department for the Ecology of Animal Societies, Max Planck Institute of Animal Behavior, Germany.

Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Brazil

Email: mauricio.cantor at oregonstate dot edu

##### Version 0.0.4 

Feb 18 2022

**Abstract**

Social foraging decisions depend on individual payoffs. However, it is unclear how individual variation in phenotypic and behavioural traits can influence these payoffs, thereby the decisions to forage socially or individually. Here we studied how individual traits influence foraging tactics of net-casting fishers who interact with wild dolphins. While net-casting is primarily an individual activity, in the traditional fishery with dolphins, fishers can choose between fishing in cooperative groups or solitarily. Our semi-structured interviews with fishers show their social network is mapped onto these foraging tactics. By quantifying the fishers’ catch, we found that fishers in cooperative groups catch more fish per capita than solitary fishers. By quantifying foraging and social traits of fishers, we found that the choice between foraging tactics—and whom to cooperate with—relates to differences in peer reputation and to similarities in number of friends, propensity to fish with relatives, and frequency of interaction with dolphins. These findings indicate different payoffs between foraging tactics and that by choosing the cooperative partner fishers likely access other benefits such as social prestige and embeddedness. These findings reveal the importance of not only material but also non-material benefits of social foraging tactics, which can have implications for the dynamics of this rare fishery. Faced with the current fluctuation in fishing resource availability, the payoffs of both tactics may change, affecting the fishers’ social and foraging decisions, potentially threatening the persistence of this century-old fishery involving humans and wildlife.


#### REFERENCE

This is a supplementary material of the  article [Santos-Silva et al. 2022](https://link.springer.com/article/10.1007/s00265-022-03152-2) published in the journal Behavioural Ecology and Sociobiology: 

```
Santos-Silva B, Hanazaki N, Daura-Jorge FG & Cantor M. 2022. 
Social foraging can benefit artisanal fishers who interact with wild dolphins. 
Behavioural Ecology and Sociobiology, 76(42):1-14.
DOI: https://doi.org/10.1007/s00265-022-03152-2

```

If you use the code, data or functions, please cite the article above and this repository. Thank you!