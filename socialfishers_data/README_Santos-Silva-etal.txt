# ----------------------------------------------------------------------------- #
#                                                                  		#
# Social foraging can benefit artisanal fishers who interact with wild dolphins #
#                                                                  		#
# Bruna Santos Silva, Natalia Hanazaki1, Fabio Daura-Jorge, Mauricio Cantor     #
#                                                                  		#
# correspondence: mauricio.cantor"AT"oregonstate.edu                            #
#                                                                  		#
#                 --- READ-ME for the empirical data ----                	#
#                                                                  		#
# Bitbucket code repository:                                       		#
# https://bitbucket.org/maucantor/social_fishers/src/master/			#
# Bitbucket data repository:                                       		#
# https://bitbucket.org/maucantor/social_fishers/src/master/socialfishers_data/ #
#                                                                  		#
# If you use (part of) this data or code, please cite the article above and 	#
# its associated code and data package.          				#
# Thank you.                                                      		#
#-------------------------------------------------------------------------------#




# 1. Empirical data file 1 ----


* Object: data_cast_anonymous

* Description: Raw data on individual traits of interviewed individual artisanal fishers; each unit is a fisher

* Type: alphanumeric data frame, 49 obs. of  15 variables:
 $ NodeID            : int  1 2 3 4 5 6 7 8 9 10 ...
 $ fisher            : chr  "fisher1" "fisher2" "fisher3" "fisher4" ...
 $ modulo_number     : int  1 NA NA 1 NA NA 2 NA 3 1 ...
 $ modulo_size       : int  10 1 1 10 1 1 3 1 6 10 ...
 $ idade             : int  68 56 44 59 59 49 55 45 37 30 ...
 $ experience        : int  53 42 26 39 49 24 41 33 28 17 ...
 $ economia          : int  0 0 0 0 0 1 1 0 0 1 ...
 $ parentesco        : int  1 1 0 1 0 0 0 0 1 1 ...
 $ fpSafra           : int  10 10 4 4 1 4 10 10 1 10 ...
 $ fpAno             : int  10 4 4 10 10 10 10 10 10 10 ...
 $ reputacao         : int  40 0 0 0 0 0 2 0 0 15 ...
 $ amigos            : int  5 0 0 0 0 2 1 0 0 9 ...
 $ cooperation_degree: int  9 0 0 4 0 0 2 0 2 6 ...
 $ perceived_benefit : int  50 0 0 40 0 0 40 0 50 50 ...
 $ strategy          : chr  "cooperative" "noncooperative" "noncooperative" "cooperative" ...

* Variable description:
$ NodeID            : numeric individual ID for each anonymized fisher in the social network
$ fisher            : alphanumeric individual ID for each anonymized fisher
$ modulo_number     : numeric label for each cooperative fishing group defined by modules in the cooperative network
$ modulo_size       : number of fishers in each cooperative fishing group
$ idade             : fishers’ age, in years, as a proxy for accumulated fishing practice
$ experience        : fishers' expertise in interacting with dolphins, measured in years of practice net-casting with dolphins, as a proxy for their ability to understand the dolphins’ behaviour and react properly
$ economia          : economic dependency on fishing, as Boolean variable: whether individuals were full-time (1) or part-time fishers (0)
$ parentesco        : binary proxy of genetic kinship, whether each fisher reported to be a close relative or genetically unrelated to the other local fishers
$ fpSafra           : fishing frequency during the mullet season, nnumber of weeks dedicated to net-casting with dolphins
$ fpAno             : yearly fishing frequency (number of months per year dedicated to net-casting with dolphins
$ reputacao         : peer reputation, measured by how many times each fisher was cited by their peers as being the most skilled or successful in fishing with dolphins
$ amigos            : friendship connectedness, measured by how many times each fisher was cited by their peers as being their close friends
$ cooperation_degree: individual network degree in the cooperative network (i.e., number of links (cooperative interactions) of each node (fisher))
$ perceived_benefit : perceived benefit of cooperating with other fishers coded into five numerical categories (e.g. no benefit=0, shared spot=20, shared fish caught=30, shared money of fish sold =40, shared spot and money of fish sold=50
$ strategy          : foraging tactic, either cooperative (coop) or solitary (noncoop)






# 2. Empirical data file 2 ----


* Object: data_net_coopera_ampla

* Description: We built a social network of fishers based on their answers to the question on which fishers they cooperate with. Here we provide the 49-by-49 binary and symmetric adjacency matrix where each row and column is a net-casting fisher (number-coded), and  the element aij = 1 when fisher i declared to cooperate with j or fisher j declared to cooperate with i; and aij = 0 when neither i or j declared cooperation. 











# 3. Empirical data file 3 ----


* Object: data_cast_anonymous

* Description: Raw data on net-casting events by interviewed individual artisanal fishers; each unit is a net-casting event

* Type: alphanumeric data frame, 169 obs. of  8 variables
 $ NodeID           : int  1 1 1 1 1 1 1 1 1 1 ...
 $ fisher           : chr  "fisher1" "fisher1" "fisher1" "fisher1" ...
 $ module_number    : int  1 1 1 1 1 1 1 1 1 1 ...
 $ modulo_size      : int  10 10 10 10 10 10 10 10 10 10 ...
 $ fish_caught      : int  1 51 1 1 1 1 1 1 2 1 ...
 $ biomass_percapita: num  0.1 5.1 0.1 0.1 0.1 0.1 0.1 0.1 0.2 0.1 ...
 $ mean_fish_length : num  49 43.7 35 37 44 41 36 37 37 51 ...
 $ strategy         : chr  "coopera" "coopera" "coopera" "coopera" ...

* Variable description:
$ NodeID           : numeric individual ID for each anonymized fisher
$ fisher           : alphanumeric individual ID for each anonymized fisher
$ module_number    : numeric label for each cooperative fishing group defined by modules in the cooperative network
$ modulo_size      : number of fishers in each cooperative fishing group
$ fish_caught      : number of mullet fish (Mugil liza) caught
$ biomass_percapita: biomass (weight in kg * number of fish) divided by the number of fishers in each group (modulo_size)
$ mean_fish_length : averaged length in cm (tip of head to tip of tail) of all mullet fish caught in each casting event
$ strategy         : foraging tactic, either cooperative (coop) or solitary (noncoop)









# 4. Empirical data file 4 ----


* Object: data_cast_all_anonymous

* Description: Complete data on net-casting events by all observed individual artisanal fishers (interviewed or not); each unit is a net-casting event

* Type: alphanumeric data frame, 291 obs. of  13 variables
 $ date             : chr  "2019-06-09" "2019-07-12" "2019-06-05" "2019-06-05" ...
 $ time             : chr  "14:24:23" NA "10:34:32" "14:40:27" ...
 $ year             : int  2019 2019 2019 2019 2019 2019 2019 2019 2019 2019 ...
 $ band             : int  0 0 0 0 0 0 0 0 7 0 ...
 $ NodeID           : int  NA NA NA NA 39 39 39 39 39 NA ...
 $ fisher           : chr  "fisherNA" "fisherNA" "fisherNA" "fisherNA" ...
 $ module_number    : int  0 0 0 0 2 2 2 2 2 0 ...
 $ modulo_size      : int  1 1 1 1 3 3 3 3 3 1 ...
 $ fish_caught      : int  1 4 1 1 1 2 1 1 1 5 ...
 $ biomass_percapita: num  1 4 1 1 0.33 0.67 0.33 0.33 0.33 5 ...
 $ mean_fish_length : num  48 45 40 45 38 35.5 30 30 46 34.8 ...
 $ strategy         : chr  "noncoop" "noncoop" "noncoop" "noncoop" ...
 $ interview        : int  0 0 0 0 1 1 1 1 1 0 ...

* Variable description:
$ date             : date in yyy-mm-dd format
$ time             : time in hh:mm:ss format
$ year             : year in yyyy format
$ band             : individual GPS band identifier
$ NodeID           : numeric individual ID for each anonymized fisher
$ fisher           : alphanumeric individual ID for each anonymized fisher
$ module_number    : numeric label for each cooperative fishing group defined by modules in the cooperative network
$ modulo_size      : number of fishers in each cooperative fishing group
$ fish_caught      : number of mullet fish (Mugil liza) caught
$ biomass_percapita: biomass (weight in kg * number of fish) divided by the number of fishers in each group (modulo_size)
$ mean_fish_length : averaged length in cm (tip of head to tip of tail) of all mullet fish caught in each casting event
$ strategy         : foraging tactic, either cooperative (coop) or solitary (noncoop)
$ interview        : Boolean, whether the fisher was interviewed or not






